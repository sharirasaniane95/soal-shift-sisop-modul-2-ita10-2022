#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>
#include <string.h>

void insertionSort(int arrTahun[5][1], char arrFilename[5][100], int n)
{
    int i, key, j;
    char kuy[100];
    for (i = 1; i < n; i++)
    {
        key = arrTahun[i][0];
        strcpy(kuy, arrFilename[i]);
        j = i - 1;

        while (j >= 0 && arrTahun[j][0] > key)
        {
            arrTahun[j + 1][0] = arrTahun[j][0];
            strcpy(arrFilename[j + 1], arrFilename[j]);
            j = j - 1;
        }
        arrTahun[j + 1][0] = key;
        strcpy(arrFilename[j + 1], kuy);
    }
}


void renameFile(char *whereFolder)
{
    DIR *dp;
    struct dirent *ep;
    char path[100] = "/home/firdho/shift2/drakor/";
    strcat(path, whereFolder);

    dp = opendir(path);

    if(dp != NULL){
        int arrTahun[5][1] = {};
        char arrFileName[5][100] = {};
        int idx = 0;

        while ((ep = readdir(dp))){
            if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
                int i = 0;
                char arr[5][50] = {};
                char fileName[100];
                char backupFilename[100];
                strcpy(fileName, ep->d_name);
                strcpy(backupFilename, ep->d_name);
                char *token = strtok(ep->d_name, ";");

                while (token != NULL)
                {
                    strcpy(arr[i], token);
                    i++;
                    token = strtok(NULL, ";");
                }

                if(i == 3){
                    pid_t child_id;
                    child_id = fork();

                    if(child_id < 0){
                        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
                    } 

                    char corePath[100] = "/home/firdho/shift2/drakor/";
                    strcat(corePath, whereFolder);
                    strcat(corePath, "/");

                    char destination[100];
                    char fileSources[100];

                    // copy the corePath
                    strcpy(destination, corePath);
                    strcpy(fileSources, corePath);

                    // define file sources
                    strcat(fileSources, fileName);

                    // define destination
                    strcat(destination, arr[0]);
                    strcat(destination, ".png");

                    arrTahun[idx][0] = atoi(arr[1]);
                    strcpy(arrFileName[idx], arr[0]);
                    idx++;

                    if(child_id == 0){
                        char *moveFile3[] = {"mv", fileSources, destination, NULL};

                        execv("/usr/bin/mv", moveFile3);
                        exit(EXIT_SUCCESS);
                    }
                } else if (i == 5) {
                    pid_t child_id;
                    child_id = fork();

                    if(child_id < 0){
                        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
                    } 

                    char corePath[100] = "/home/firdho/shift2/drakor/";
                    strcat(corePath, whereFolder);
                    strcat(corePath, "/");

                    char destination[100];
                    char fileSources[100];

                    // copy the corePath
                    strcpy(destination, corePath);
                    strcpy(fileSources, corePath);

                    // define file sources
                    strcat(fileSources, fileName);

                    char *file53 = strtok(arr[2], "_");
                    char *file55 = strtok(arr[4], ".");

                    if (strcmp(file53, whereFolder) == 0)
                    {
                        // define destination
                        strcat(destination, arr[0]);
                        strcat(destination, ".png");

                        arrTahun[idx][0] = atoi(arr[1]);
                        strcpy(arrFileName[idx], arr[0]);
                        idx++;
                    }
                    else if (strcmp(file55, whereFolder) == 0)
                    {
                        char *token52 = strtok(backupFilename, "_");
                        char *token522 = strtok(token52, ";");

                        // define destination
                        strcat(destination, token522);
                        strcat(destination, ".png");

                        arrTahun[idx][0] = atoi(arr[3]);
                        strcpy(arrFileName[idx], token522);
                        idx++;
                    }

                    if(child_id == 0){
                        char *moveFile[] = {"mv", fileSources, destination, NULL};

                        execv("/usr/bin/mv", moveFile);
                        exit(EXIT_SUCCESS);
                    }
                }
            }
        }

        strcat(path, "/data.txt");

        pid_t child_id;
        child_id = fork();

        if(child_id < 0){
            exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
        } 

        if(child_id == 0){
            char *createTxt[] = {"touch", path, NULL};

            execv("/usr/bin/touch", createTxt);
            exit(EXIT_SUCCESS);
        }

        FILE *f = fopen(path, "w");
        if (f == NULL)
        {
            printf("Error when opening file %s \n", path);
            exit(1);
        }

        fprintf(f, "kategori: %s \n\n", whereFolder);

        insertionSort(arrTahun, arrFileName, 5);

        for(int i = 0; i < 5; i++){
            if (arrTahun[i][0] != 0){
                fprintf(f, "nama: %s \n", arrFileName[i]);
                fprintf(f, "rilis: tahun %d \n\n", arrTahun[i][0]);
            }
        }
        (void)closedir(dp);
    }
}

void getList()
{
    DIR *dp;
    struct dirent *ep;

    dp = opendir("/home/firdho/shift2/drakor/");

    if (dp != NULL){
        while((ep = readdir(dp))){
            if(strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
                renameFile(ep->d_name);
            }
        }
    }
    (void)closedir(dp);
}

void moveFile()
{
    DIR *dp;
    struct dirent *ep;
    int status;

    dp = opendir("/home/firdho/shift2/drakor/");

    if(dp != NULL){
        while ((ep = readdir(dp))){
            if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
                int i = 0;
                char arr[5][50] = {};
                char fileName[100];
                strcpy(fileName, ep->d_name);
                char *token = strtok(ep->d_name, ";");

                while (token != NULL){
                    strcpy(arr[i], token);
                    i++;
                    token = strtok(NULL, ";");
                }

                if(i == 3){
                    pid_t child_id;
                    child_id = fork();

                    if(child_id < 0){
                        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
                    }

                    char *token3 = strtok(arr[2], ".");
                    char corePath[100] = "/home/firdho/shift2/drakor/";
                    char destination[100];
                    char fileSources[100];

                    // copy the corePath
                    strcpy(destination, corePath);
                    strcpy(fileSources, corePath);

                    // define destination
                    strcat(destination, token3);
                    strcat(destination, "/");

                    // define file sources
                    strcat(fileSources, fileName);

                    if(child_id == 0){
                        char *moveFile3[] = {"mv", fileSources, destination, NULL};

                        execv("/usr/bin/mv", moveFile3);
                        exit(EXIT_SUCCESS);
                    }
                } else if (i == 5) {
                    pid_t child_id;
                    child_id = fork();

                    if(child_id < 0){
                        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
                    }

                    char *token53 = strtok(arr[2], "_");
                    char *token55 = strtok(arr[4], ".");
                    char corePath[100] = "/home/firdho/shift2/drakor/";
                    char destination1[100];
                    char destination2[100];
                    char fileSources1[100];
                    char fileSources2[100];

                    // copy the corePath
                    strcpy(destination1, corePath);
                    strcpy(destination2, corePath);
                    strcpy(fileSources1, corePath);
                    strcpy(fileSources2, corePath);

                    // define file sources
                    strcat(fileSources1, fileName);
                    strcat(fileSources2, fileName);

                    // define destination1
                    strcat(destination1, token53);
                    strcat(destination1, "/");

                    // define destination2
                    strcat(destination2, token55);
                    strcat(destination2, "/");

                    if(child_id == 0){
                        char *copyFile53[] = {"cp", fileSources1, destination1, NULL};
                        char *moveFile55[] = {"mv", fileSources2, destination2, NULL};

                        execv("/usr/bin/cp", copyFile53);
                        execv("/usr/bin/mv", moveFile55);
                        exit(EXIT_SUCCESS);
                    }
                }
            }
        }
    }
    (void)closedir(dp);
    while ((wait(&status)) > 0);
    getList();
}


void createFolder(){
    DIR *dp;
    struct dirent *ep;
    int status;

    dp = opendir("/home/firdho/shift2/drakor/");

    if(dp != NULL){
        while ((ep = readdir(dp))){
            if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
                int i = 0;
                char arr[5][50] = {};
                char *token = strtok(ep->d_name, ";");

                while(token != NULL){
                    strcpy(arr[i], token);
                    i++;
                    token = strtok(NULL, ";");
                }

                if(i == 3){   
                    pid_t child_id;
                    child_id = fork();

                    if(child_id < 0){
                        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
                    }

                    char *token3 = strtok(arr[2], ".");
                    char corePath[100] = "/home/firdho/shift2/drakor/";
                    strcat(corePath, token3);

                    if(child_id == 0){
                        char *createFolder3[] = {"mkdir", "-p", corePath, NULL};

                        execv("/bin/mkdir", createFolder3);
                        exit(EXIT_SUCCESS);
                    }
                } else if (i == 5) {
                    pid_t child_id;
                    child_id = fork();

                    if(child_id < 0){
                        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
                    }

                    char *token53 = strtok(arr[2], "_");
                    char *token55 = strtok(arr[4], ".");
                    char corePath1[100] = "/home/firdho/shift2/drakor/";
                    char corePath2[100] = "/home/firdho/shift2/drakor/";
                    strcat(corePath1, token53);
                    strcat(corePath2, token55);
                    
                    
                    if(child_id == 0){
                        char *createFolder53[] = {"mkdir", "-p", corePath1, NULL};
                        char *createFolder55[] = {"mkdir", "-p", corePath2, NULL};

                        execv("/bin/mkdir", createFolder53);
                        execv("/bin/mkdir", createFolder55);
                    } 
                }
            }
        }
    }
    (void)closedir(dp);
    while ((wait(&status)) > 0);
    moveFile();
}

void delete(){
    pid_t child_id;
    child_id = fork();
    int status;

    char folder1[100] = "/home/firdho/shift2/drakor/coding";
    char folder2[100] = "/home/firdho/shift2/drakor/song";
    char folder3[100] = "/home/firdho/shift2/drakor/trash";


    if(child_id < 0){
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if(child_id == 0){
        char *argv[] = {"rm", "-r", folder1, folder2, folder3, NULL};

        execv("/usr/bin/rm", argv);
    } else {
        while ((wait(&status)) > 0);
        createFolder();
    }
}

void extract(char nameFile[], char directory[]){
    pid_t child_id;
    child_id = fork();
    int status;

    if(child_id < 0){
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if(child_id == 0){
        char *argv[] = {"unzip", "-q", nameFile, "-d", directory, NULL};

        execv("/usr/bin/unzip", argv);
    } else {
        while ((wait(&status)) > 0);
        delete();
    }
}


int main(void)
{
    pid_t child_id;

    child_id = fork();

    char directory[100] = "/home/firdho/shift2/drakor/";
    int status;

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if (child_id == 0) {
        // this is child
        FILE *file;
        if(file = fopen(directory, "r")){
            fclose(file);
            printf("Folder sudah ada \n");
        }
        else{
            printf("Folder belum ada \n");

            char *argv[] = {"mkdir", "-p", directory, NULL};
            execv("/bin/mkdir", argv);
        }
    } else {
        // this is parent
        while ((wait(&status)) > 0);
        
        char nameFile[100] = "/home/firdho/Downloads/drakor.zip";

        extract(nameFile, directory);
    }
     
    return 0;
}