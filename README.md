# :zap: **soal-shift-sisop-modul-2-ITA10-2022** :zap:

| Nama                      | NRP        |
|---------------------------|------------|
| Muhammad Firdho Kustiawan | 5027201005 |
| Sharira Saniane           | 5027201016 |
| Jovan Surya Bako          | 5027201013 | 
<br/>



## :large_blue_circle: **Soal 1** :large_blue_circle: 

Mas Refadi adalah seorang wibu gemink.  Dan jelas game favoritnya adalah bengshin impek. Terlebih pada game tersebut ada sistem gacha item yang membuat orang-orang selalu ketagihan untuk terus melakukan nya. Tidak terkecuali dengan mas Refadi sendiri. Karena rasa penasaran bagaimana sistem gacha bekerja, maka dia ingin membuat sebuah program untuk men-simulasi sistem history gacha item pada game tersebut. Tetapi karena dia lebih suka nge-wibu dibanding ngoding, maka dia meminta bantuanmu untuk membuatkan program nya. Sebagai seorang programmer handal, bantulah mas Refadi untuk memenuhi keinginan nya itu. 

## **1a**
 Saat program pertama kali berjalan. Program akan mendownload file characters dan file weapons dari link yang ada dibawah, lalu program akan mengekstrak kedua file tersebut. File tersebut akan digunakan sebagai database untuk melakukan gacha item characters dan weapons. Kemudian akan dibuat sebuah folder dengan nama “gacha_gacha” sebagai working directory. Seluruh hasil gacha akan berada di dalam folder tersebut. 

dibuat dwDB untuk mendownload database yang diperlukan yaitu character dan weapon
```c
void dwDB(char *database, char *name) {
    pid_t anak_id;
    anak_id = fork();

    if (anak_id == 0) {
        char *argv[8] = {
            "wget", 
            "--no-check-certificate", 
            "-q",
            database, 
            "o",
            name
        };
        
        execv("/bin/wget", argv); 
        exit(EXIT_SUCCESS);
    } else if (anak_id > 0) {
        wait(NULL); 
    }
}
```
selanjutnya dikarenakan file yang di didownload dalam format zip maka harus di extrak terlebih dahulu
```c

void extract(char *name) {
    pid_t anak_id;
    anak_id = fork();

    if (anak_id == 0) {
        char *argv[4] = {
            "unzip", 
            "-q",
            name
        };

        execv("/usr/bin/unzip", argv);
        exit(EXIT_SUCCESS);
    } else if (anak_id > 0) {
        wait(NULL); 
    }
}
```
setelah di extrak akan langsung di hapus 
```c
void rm(char *name) {
    pid_t anak_id;
    anak_id = fork();

    if (anak_id == 0) {
        char *argv[4] = {
            "rm", 
            "-r",
            name
        };
        execv("/usr/bin/rm", argv);
        exit(EXIT_SUCCESS);
    } else if (anak_id > 0) {
        wait(NULL); 
    }
}

```
diperlukan juga sebuah folder akan akan menyimpan hasil gacha yang bernamakan gacha_gacha
```c
void makedir(char *fold) {
    pid_t anak_id;
    anak_id = fork();

    if (anak_id == 0) {
        char *argv[2] = {
            "mkdir", 
            fold
        };
        execv("/usr/bin/mkdir", argv);
        exit(EXIT_SUCCESS);
    } else if (anak_id > 0) {
        wait(NULL); 
    }
}
```
untuk main nya dibuat seperti ini
```c
int main(){
    char *database[2] = {"https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp","https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT"};
    char *name[2] = {"uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp","uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT"};
     char *fold[1] = {"gacha_gacha"};
     makedir(fold[0]);

for (int i=0; i<2; i++){
    pid_t anak_id;
    anak_id =fork();

    if (anak_id ==0){
    dwDB(database[i], name[i]);
    extract(name[i]);
        exit(EXIT_SUCCESS);

    } else if (anak_id > 0) {
        wait(NULL); 
    }
        rm(name[i]);
}
}
```
link untuk database akan disimpan pada char juga ada char name hasil dari file yang telah didownload
disini kami menggunakan for loop untuk mendownload,ekstrak databasenya dan juga menghapus zip databasenya
berikut adalah hasil dari 1a
![output 1a](/img/1a.png)



## :large_blue_circle: **Soal 2** :large_blue_circle: 
Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun untuk menyelesaikan pekerjaannya.

**Catatan :**
- File zip berada dalam drive modul shift ini bernama drakor.zip
- File yang penting hanyalah berbentuk .png
- Setiap foto poster disimpan sebagai nama foto dengan format [nama]:[tahun rilis]:[kategori]. Jika terdapat lebih dari satu drama dalam poster, dipisahkan menggunakan underscore(_)
- Tidak boleh menggunakan fungsi system(), mkdir(), dan rename() yang tersedia di bahasa C
- Gunakan bahasa pemrograman C (Tidak boleh yang lain).
- Folder shift2, drakor, dan kategori dibuatkan oleh program (Tidak Manual).
- [user] menyesuaikan nama user linux di os anda.

Pertama-tama yang harus dilakukan yaitu membuat program untuk membuat folder shift2 yang didalamnya terdapat folder drakor, seperti berikut `/home/firdho/shift2/drakor`. Disini menggunaka 'fopen' untuk mengecek apakah kedua folder tersebut telah dibuat atau belum. Apabila folder belum dibuat maka akan menampilkan `Folder belum ada` tetapi jika sudah ada maka sebaliknya. Kemudian akan masuk pada fungsi soal no 2a untuk mengextract zip yang telah ditentukan.
```c
int main(void)
{
    pid_t child_id;

    child_id = fork();

    char directory[100] = "/home/firdho/shift2/drakor/";
    int status;

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if (child_id == 0) {
        // this is child
        FILE *file;
        if(file = fopen(directory, "r")){
            fclose(file);
            printf("Folder sudah ada \n");
        }
        else{
            printf("Folder belum ada \n");

            char *argv[] = {"mkdir", "-p", directory, NULL};
            execv("/bin/mkdir", argv);
        }
    } else {
        // this is parent
        while ((wait(&status)) > 0);
        
        char nameFile[100] = "/home/firdho/Downloads/drakor.zip";

        extract(nameFile, directory);
    }
     
    return 0;
}
```



## **2a** 
Pada soal no 2a ini kami mengextract zip drakor.zip yang telah didownload ke dalam folder `/home/firdho/shift2/drakor` .
```c
void extract(char nameFile[], char directory[]){
    pid_t child_id;
    child_id = fork();
    int status;

    if(child_id < 0){
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if(child_id == 0){
        char *argv[] = {"unzip", "-q", nameFile, "-d", directory, NULL};

        execv("/usr/bin/unzip", argv);
    } else {
        while ((wait(&status)) > 0);
        delete();
    }
}

```
Berikut merupakan output dari soal no 2a bagian extraxt drakor.zip
![output 2-1](/img/2-1.png)

Setelah diextract kemudian menghapus semua folder yang ada di dalam folder drakor tersebut dengan mendefinisikan setiap letak dari folder tersebut secara spesifik. Dapat dilihat di output sebelumnya terdapat 3 folder maka ke 3 folder tersebut-lah yang harus dihapus.
```c
void delete(){
    pid_t child_id;
    child_id = fork();
    int status;

    char folder1[100] = "/home/firdho/shift2/drakor/coding";
    char folder2[100] = "/home/firdho/shift2/drakor/song";
    char folder3[100] = "/home/firdho/shift2/drakor/trash";


    if(child_id < 0){
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if(child_id == 0){
        char *argv[] = {"rm", "-r", folder1, folder2, folder3, NULL};

        execv("/usr/bin/rm", argv);
    } else {
        while ((wait(&status)) > 0);
        createFolder();
    }
}
```
Berikut merupakan output dari soal no 2a bagian menghapus folder-folder di dalam folder drakor
![output 2-2](/img/2-2.png)


## **2b** (revisi)
Pada bagian 2b ini kami mengelompokkan poster-poster berdasarkan kategorinya masing-masing yang terletak di nama file poster tersebut sendiri dengan membuat folder untuk setiap kategori dari poster-poster tersebut. Caranya yaitu membaca seluruh isi folder drakor dengan opendir kemudian dibuat masing-masing folder dengan menjalankan perintah `mkdir` dengan `execv`.
```c
void createFolder(){
    DIR *dp;
    struct dirent *ep;
    int status;

    dp = opendir("/home/firdho/shift2/drakor/");

    if(dp != NULL){
        while ((ep = readdir(dp))){
            if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
                int i = 0;
                char arr[5][50] = {};
                char *token = strtok(ep->d_name, ";");

                while(token != NULL){
                    strcpy(arr[i], token);
                    i++;
                    token = strtok(NULL, ";");
                }

                if(i == 3){   
                    pid_t child_id;
                    child_id = fork();

                    if(child_id < 0){
                        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
                    }

                    char *token3 = strtok(arr[2], ".");
                    char corePath[100] = "/home/firdho/shift2/drakor/";
                    strcat(corePath, token3);

                    if(child_id == 0){
                        char *createFolder3[] = {"mkdir", "-p", corePath, NULL};

                        execv("/bin/mkdir", createFolder3);
                        exit(EXIT_SUCCESS);
                    }
                } else if (i == 5) {
                    pid_t child_id;
                    child_id = fork();

                    if(child_id < 0){
                        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
                    }

                    char *token53 = strtok(arr[2], "_");
                    char *token55 = strtok(arr[4], ".");
                    char corePath1[100] = "/home/firdho/shift2/drakor/";
                    char corePath2[100] = "/home/firdho/shift2/drakor/";
                    strcat(corePath1, token53);
                    strcat(corePath2, token55);
                    
                    
                    if(child_id == 0){
                        char *createFolder53[] = {"mkdir", "-p", corePath1, NULL};
                        char *createFolder55[] = {"mkdir", "-p", corePath2, NULL};

                        execv("/bin/mkdir", createFolder53);
                        execv("/bin/mkdir", createFolder55);
                    } 
                }
            }
        }
    }
    (void)closedir(dp);
    while ((wait(&status)) > 0);
    moveFile();
}
```
Berikut merupakan output dari 2b.
![output 2-3](/img/2-3.png)

## **2c dan 2d** (revisi)
Pada bagian 2c ini yaitu memindahkan poster-poster yang ada ke folder sesuai kategorinya masing-masing yaitu dengan mambaca seluruh isi dari folder drakor kemudian setiap nama file akan dibaca juga sekaligus difilter hingga tanda `;` dengan menggunakan `strtok`. Setelah itu apabila dalam pembacaan tersebut nama file posternya jumlah katanya 3 maka akan langsung masuk ke folder yang sesuai tetapi apabila dalam nama file posternya jumlah aktanya 5 maka akan dimasukkan ke dalam ke 2 folder. Contoh:
`love-alarm;2019;romance_who-are-you;2015;school.png`
setiap penggalan dari `;` akan terhitung 1 dan kata ke 3 (romance_...) serta kata ke 5 (school) akan menentukan file poster tersebut akan dipindah ke 2 folder yang sesuai dengan tujuan folder pertama dari kata ke 3 akan menggunakan perintah `copy` sedangkan untuk tujuan folder kedua dari kata ke 5 akan menggunakan perintah `move`.

```c
void moveFile()
{
    DIR *dp;
    struct dirent *ep;
    int status;

    dp = opendir("/home/firdho/shift2/drakor/");

    if(dp != NULL){
        while ((ep = readdir(dp))){
            if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
                int i = 0;
                char arr[5][50] = {};
                char fileName[100];
                strcpy(fileName, ep->d_name);
                char *token = strtok(ep->d_name, ";");

                while (token != NULL){
                    strcpy(arr[i], token);
                    i++;
                    token = strtok(NULL, ";");
                }

                if(i == 3){
                    pid_t child_id;
                    child_id = fork();

                    if(child_id < 0){
                        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
                    }

                    char *token3 = strtok(arr[2], ".");
                    char corePath[100] = "/home/firdho/shift2/drakor/";
                    char destination[100];
                    char fileSources[100];

                    // copy the corePath
                    strcpy(destination, corePath);
                    strcpy(fileSources, corePath);

                    // define destination
                    strcat(destination, token3);
                    strcat(destination, "/");

                    // define file sources
                    strcat(fileSources, fileName);

                    if(child_id == 0){
                        char *moveFile3[] = {"mv", fileSources, destination, NULL};

                        execv("/usr/bin/mv", moveFile3);
                        exit(EXIT_SUCCESS);
                    }
                } else if (i == 5) {
                    pid_t child_id;
                    child_id = fork();

                    if(child_id < 0){
                        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
                    }

                    char *token53 = strtok(arr[2], "_");
                    char *token55 = strtok(arr[4], ".");
                    char corePath[100] = "/home/firdho/shift2/drakor/";
                    char destination1[100];
                    char destination2[100];
                    char fileSources1[100];
                    char fileSources2[100];

                    // copy the corePath
                    strcpy(destination1, corePath);
                    strcpy(destination2, corePath);
                    strcpy(fileSources1, corePath);
                    strcpy(fileSources2, corePath);

                    // define file sources
                    strcat(fileSources1, fileName);
                    strcat(fileSources2, fileName);

                    // define destination1
                    strcat(destination1, token53);
                    strcat(destination1, "/");

                    // define destination2
                    strcat(destination2, token55);
                    strcat(destination2, "/");

                    if(child_id == 0){
                        char *copyFile53[] = {"cp", fileSources1, destination1, NULL};
                        char *moveFile55[] = {"mv", fileSources2, destination2, NULL};

                        execv("/usr/bin/cp", copyFile53);
                        execv("/usr/bin/mv", moveFile55);
                        exit(EXIT_SUCCESS);
                    }
                }
            }
        }
    }
    (void)closedir(dp);
    while ((wait(&status)) > 0);
    getList();
}
```
Berikut merupakan output dari 2c bagian memindahkan file sesuai folder.
![output 2-4](/img/2-4.png)

## **2c dan 2e** (revisi)
Pada bagian ini masih terdapat kaitan dengan program sebelumnya untuk memindahkan setiap poster ke dalam folder yang sesuai tetapi ditambahkan program untuk membuat file `data.txt` yang akan menyimpan kategori folder tersebut, nama poster yang ada di dalam folder tersebut, dan tahun rilis. Caranya yaitu dengan membuat sebuah fungsi untuk melakukan list `getList` semua isi folder drakor dengan mengambil setiap nama file dengan ketentuan terdapat `.`. Kemudian akan diarahkan ke fungsi untuk merename setiap file yang telah diambil parameternya dari `getList` untuk dijadikan parameter `renameFile`. Kemudian sama seperti sebelumnya apabila nama file tersebut hanya terdiri dari 3 kata maka akan direname sesuai kata pertamanya sedangkan kata selanjutnya akan disimpan pada array tetapi apabila terdiri dari 5 kata maka akan direname sesuai kata pertamanya akan tetapi dicek terlebih dahulu apakah foldernya sudah sesuai atau belum. Kemudian akan melakukan pengurutan secara ascending dan mencetak file `data.txt`
```c
void insertionSort(int arrTahun[5][1], char arrFilename[5][100], int n)
{
    int i, key, j;
    char kuy[100];
    for (i = 1; i < n; i++)
    {
        key = arrTahun[i][0];
        strcpy(kuy, arrFilename[i]);
        j = i - 1;

        while (j >= 0 && arrTahun[j][0] > key)
        {
            arrTahun[j + 1][0] = arrTahun[j][0];
            strcpy(arrFilename[j + 1], arrFilename[j]);
            j = j - 1;
        }
        arrTahun[j + 1][0] = key;
        strcpy(arrFilename[j + 1], kuy);
    }
}


void renameFile(char *whereFolder)
{
    DIR *dp;
    struct dirent *ep;
    char path[100] = "/home/firdho/shift2/drakor/";
    strcat(path, whereFolder);

    dp = opendir(path);

    if(dp != NULL){
        int arrTahun[5][1] = {};
        char arrFileName[5][100] = {};
        int idx = 0;

        while ((ep = readdir(dp))){
            if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
                int i = 0;
                char arr[5][50] = {};
                char fileName[100];
                char backupFilename[100];
                strcpy(fileName, ep->d_name);
                strcpy(backupFilename, ep->d_name);
                char *token = strtok(ep->d_name, ";");

                while (token != NULL)
                {
                    strcpy(arr[i], token);
                    i++;
                    token = strtok(NULL, ";");
                }

                if(i == 3){
                    pid_t child_id;
                    child_id = fork();

                    if(child_id < 0){
                        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
                    } 

                    char corePath[100] = "/home/firdho/shift2/drakor/";
                    strcat(corePath, whereFolder);
                    strcat(corePath, "/");

                    char destination[100];
                    char fileSources[100];

                    // copy the corePath
                    strcpy(destination, corePath);
                    strcpy(fileSources, corePath);

                    // define file sources
                    strcat(fileSources, fileName);

                    // define destination
                    strcat(destination, arr[0]);
                    strcat(destination, ".png");

                    arrTahun[idx][0] = atoi(arr[1]);
                    strcpy(arrFileName[idx], arr[0]);
                    idx++;

                    if(child_id == 0){
                        char *moveFile3[] = {"mv", fileSources, destination, NULL};

                        execv("/usr/bin/mv", moveFile3);
                        exit(EXIT_SUCCESS);
                    }
                } else if (i == 5) {
                    pid_t child_id;
                    child_id = fork();

                    if(child_id < 0){
                        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
                    } 

                    char corePath[100] = "/home/firdho/shift2/drakor/";
                    strcat(corePath, whereFolder);
                    strcat(corePath, "/");

                    char destination[100];
                    char fileSources[100];

                    // copy the corePath
                    strcpy(destination, corePath);
                    strcpy(fileSources, corePath);

                    // define file sources
                    strcat(fileSources, fileName);

                    char *file53 = strtok(arr[2], "_");
                    char *file55 = strtok(arr[4], ".");

                    if (strcmp(file53, whereFolder) == 0)
                    {
                        // define destination
                        strcat(destination, arr[0]);
                        strcat(destination, ".png");

                        arrTahun[idx][0] = atoi(arr[1]);
                        strcpy(arrFileName[idx], arr[0]);
                        idx++;
                    }
                    else if (strcmp(file55, whereFolder) == 0)
                    {
                        char *token52 = strtok(backupFilename, "_");
                        char *token522 = strtok(token52, ";");

                        // define destination
                        strcat(destination, token522);
                        strcat(destination, ".png");

                        arrTahun[idx][0] = atoi(arr[3]);
                        strcpy(arrFileName[idx], token522);
                        idx++;
                    }

                    if(child_id == 0){
                        char *moveFile[] = {"mv", fileSources, destination, NULL};

                        execv("/usr/bin/mv", moveFile);
                        exit(EXIT_SUCCESS);
                    }
                }
            }
        }

        strcat(path, "/data.txt");

        pid_t child_id;
        child_id = fork();

        if(child_id < 0){
            exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
        } 

        if(child_id == 0){
            char *createTxt[] = {"touch", path, NULL};

            execv("/usr/bin/touch", createTxt);
            exit(EXIT_SUCCESS);
        }

        FILE *f = fopen(path, "w");
        if (f == NULL)
        {
            printf("Error when opening file %s \n", path);
            exit(1);
        }

        fprintf(f, "kategori: %s \n\n", whereFolder);

        insertionSort(arrTahun, arrFileName, 5);

        for(int i = 0; i < 5; i++){
            if (arrTahun[i][0] != 0){
                fprintf(f, "nama: %s \n", arrFileName[i]);
                fprintf(f, "rilis: tahun %d \n\n", arrTahun[i][0]);
            }
        }
        (void)closedir(dp);
    }
}

void getList()
{
    DIR *dp;
    struct dirent *ep;

    dp = opendir("/home/firdho/shift2/drakor/");

    if (dp != NULL){
        while((ep = readdir(dp))){
            if(strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
                renameFile(ep->d_name);
            }
        }
    }
    (void)closedir(dp);
}
```
Berikut merupakan outputnya.
![output 2-5](/img/2-5.png)
![output 2-6](/img/2-6.png)


## :large_blue_circle: **Soal 3** :large_blue_circle: 
Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang

**Catatan :**
- Tidak boleh memakai system().
- Tidak boleh memakai function C mkdir() ataupun rename().
- Gunakan exec dan fork
- Direktori “.” dan “..” tidak termasuk

*library* yang akan digunakan untuk menunjang dan menjalankan program ini: 
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <dirent.h>
#include <wait.h>
#include <sys/types.h>
#include <sys/unistd.h>
#include <sys/wait.h>
```

<<<<<<< HEAD
## 3a
Conan diminta membuat program untuk membuat 2 directory di `/home/[USER]/modul2/` dengan nama `darat` lalu 3 detik kemudian membuat directory ke 2 dengan nama `air`

**Penyelesaian:**
Langkah pertama membuat variabel anak/child dari 1 - 8 :
```c
int main(void){
  pid_t anak_1;
  pid_t anak_2;
  pid_t anak_3;
  pid_t anak_4;
  pid_t anak_5;
  pid_t anak_6;
  pid_t anak_7;
  pid_t anak_8;
  int status;
```
Langkah berikutnya, kami membuat program sebagai berikut : 
```c
 //3a
    //membuat folder darat
    anak_1 = fork();
    if(anak_1 < 0) exit();

    if(anak_1 == 0){
        char *argv[] = {"mkdir", "-p", "/home/sharira/modul2/darat", NULL};
        execv("/bin/mkdir", argv);
    } else 
        while((wait(&status)) > 0);
        
    //membuat folder air
    anak_2 = fork();
    if(anak_2 < 0) exit();

    if(anak_2 == 0){
        sleep(3);
        char *argv[] = {"mkdir", "-p", "/home/sharira/modul2/air", NULL};
        execv("/bin/mkdir", argv);
    } else while((wait(&status)) > 0);
```
Program pembuatan folder darat dan air akan berjalan jika `anak == 0` jika `anak == 1` maka program tidak dapat dijalankan. Kemudian,untuk membuat folder menggunakan variabel `char *argv` dan saat ingin menyimpan folder baru menggunakan perintah `mkdir", "-p", "/home/sharira/modul2/(nama folder)` . Perintah selanjutnya ` execv("/bin/mkdir", argv)`. Terakhir untuk memberi delay selama 3 detik pada pembuatan folder darat ke air menggunakan perintah  `sleep(3)`.

## Output soal 3a
folder darat : 

![folder darat](/img/3a.darat.png)

Setelah 3 detik akan membuat folder air :

![folder air](/img/3a.png)



## 3b
Kemudian program diminta dapat melakukan extract `animal.zip” di “/home/[USER]/modul2/`

**Penyelesaian:**
Pada program ini dalam membuat unzip menggunakan perintah : 

```c
//3b  
    //extract animal.zip
    anak_3 = fork();

    if(anak_3 < 0) exit();

    if(anak_3 == 0){
        char *argv[] = {"unzip", "-oq", "/home/sharira/animal.zip", "-d", "/home/sharira/modul2/", NULL};
        execv("/usr/bin/unzip", argv);
    } else while((wait(&status)) > 0);
```
Perintah unzip menggunakan variabel `argv` pada `unzip", "-oq", "/home/sharira/animal.zip,"-d", "/home/sharira/modul2/", NULL`
- perintah `-o` digunakan untuk melakukan *overwrite* jika folder sudah ada di tempat tujuan
- perintah `-d` digunakan untuk menunjukkandirectorytempat menyimpan hasil *unzip* 
terakhir menggunakan perintah `execv("usr/bin/unzip", argv)`

### Output 3b

![hasil unzip](/img/3b.png)

## 3c (revisi)
Hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.

**Penyelesaian:**
Pada soal ini kami menggunakan perintah program : 

```c
//3c
    //dipisahkan ke darat
    anak_4 = fork();
    if(anak_4 < 0) exit();

    if(anak_4 == 0){
        execl("/usr/bin/find", "find", "/home/sharira/modul2/animal/", "-type", "f", "-name", "*darat*", "-exec", "mv", "-t", "/home/sharira/modul2/darat", "{}", "+", (char *) NULL);
    } else while((wait(&status)) > 0);
                
    //dipisahkan ke air
    anak_5 = fork();
    if(anak_5 < 0) exit();

    if(anak_5 == 0){
        sleep(3);
        execl("/usr/bin/find", "find", "/home/sharira/modul2/animal/", "-type", "f", "-name", "*air*", "-exec", "mv", "-t", "/home/sharira/modul2/air/", "{}", "+", (char *) NULL);
    } else while(wait((&status)) > 0);

```
Langkah pertama, melakukan `find` kemudian seluruh file  `*darat*` dan `*air*`. selanjutnya, `"/home/sharira/modul2/darat"` dan `"/home/sharira/modul2/air/"`. Terakhir diberikan perintah `sleep(3)` untuk membuat delay waktu sebanyak 3 detik.

Untuk menghapus file yang tersisa pada directory animal,menggunakan perintah sebagai berikut : 

```c
//menghapus file in dir: animal
    anak_6 = fork();

    if(anak_6 < 0) exit();

    if(anak_6 == 0) 
        execl("/bin/sh", "sh", "-c", "rm -f /home/sharira/modul2/animal/*", (char *) NULL);
    else while(wait((&status)) > 0);
```
Perintah `rm` merupakan perintah dalam linux untuk melakukan *remove*, `-f` atau *force*, dan terakhir menggunakan perintah untuk menghapus selueruh file yang berada pada directory animal `/home/sharira/modul2/animal/` 

### Output 3c

Folder air:

![folder air](/img/3c%20Air.png)

Folder darat:

![folder darat](/img/3c%20darat.png)

Folder animal:

![folder animal](/img/3c%20animal .png)



## 3d (revisi)
Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file.

**Penyelesaian:**
Dalam menjalalankan yang diinginkan oleh soal kami menggunakan perintah sebagai berikut : 

```c
//mengapus bird
    anak_7 = fork();
    if(anak_7 < 0) exit();

    if (anak_7 == 0){
        execl("/bin/sh", "sh", "-c", "rm -f /home/sharira/modul2/darat/*bird*", (char *) NULL);
    }   
    
    else while(wait((&status)) > 0);
```
perintah `rm` merupakan perintah dalam linux untuk melakukan *remove*, `-f` atau *force* berfungsi untuk memaksa program agar dapat langsung *remove* tanpa mengeluarkan prompt, `/home/sharira/modul2/darat/*bird*` berarti menghapus file yang memiliki tulisan **bird** pada folder darat.


### Output 3d 
folder darat tanpa ada file yang bernama bird :
![folder darat tanpa burung](/img/3d.png)



## 3e (revisi)
harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.
Contoh : conan_rwx_hewan.png

**Penyelesaian:**
Untuk soal 3e saya menggunakan perintah program sebagai berikut : 

```c
/3e
    //input file name format: uid_uid permission_filename
    anak_8 = fork();
    if(anak_8 < 0) exit();
  
    if (anak_8 == 0){
        execl("/bin/sh", "sh", "-c", "ls -la /home/sharira/modul2/air | awk 'NR > 3 {print $3\"_\"substr($1,2,2)\"_\"$9}' > /home/sharira/modul2/air/list.txt", (char *) NULL);
    }   
    
    else while(wait((&status)) > 0);
}
```
- Perintah `sh` digunakan untuk menjalankan perintah shell 
- perintah `ls -la /home/sharira/modul2/air ` berfungsi untuk mendapatkan list detail seluruh permision dan user yang terdapat dalam folder air 
-  `substr` berfungsi untuk memilih sebuah substring dari input
-  `/home/sharira/modul2/air/list.txt` berfungsi untuk nenasukkab hasil awk ke dalam file list.tx

### Output soal 3e 
File list.txt pada folder air:

![list.txt di air](/img/3e1.png)

Isi file list.txt:

![isi list.txt](/img/3e2.png)


