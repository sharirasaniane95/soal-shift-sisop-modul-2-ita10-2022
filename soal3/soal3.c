#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <dirent.h>
#include <wait.h>
#include <sys/types.h>
#include <sys/unistd.h>
#include <sys/wait.h>

int main(void){
  pid_t anak_1;
  pid_t anak_2;
  pid_t anak_3;
  pid_t anak_4;
  pid_t anak_5;
  pid_t anak_6;
  pid_t anak_7;
  pid_t anak_8;
  int status;

    //3a
    //membuat folder darat
    anak_1 = fork();
    if(anak_1 < 0) exit();

    if(anak_1 == 0){
        char *argv[] = {"mkdir", "-p", "/home/sharira/modul2/darat", NULL};
        execv("/bin/mkdir", argv);
    } else 
        while((wait(&status)) > 0);
        
    //membuat folder air
    anak_2 = fork();
    if(anak_2 < 0) exit();

    if(anak_2 == 0){
        sleep(3);
        char *argv[] = {"mkdir", "-p", "/home/sharira/modul2/air", NULL};
        execv("/bin/mkdir", argv);
    } else while((wait(&status)) > 0);

    //3b  
    //extract animal.zip
    anak_3 = fork();

    if(anak_3 < 0) exit();

    if(anak_3 == 0){
        char *argv[] = {"unzip", "-oq", "/home/sharira/animal.zip", "-d", "/home/sharira/modul2/", NULL};
        execv("/usr/bin/unzip", argv);
    } else while((wait(&status)) > 0);
    
    //3c
    //dipisahkan ke darat
    anak_4 = fork();
    if(anak_4 < 0) exit();

    if(anak_4 == 0){
        execl("/usr/bin/find", "find", "/home/sharira/modul2/animal/", "-type", "f", "-name", "*darat*", "-exec", "mv", "-t", "/home/sharira/modul2/darat", "{}", "+", (char *) NULL);
    } else while((wait(&status)) > 0);
                
    //dipisahkan ke air
    anak_5 = fork();
    if(anak_5 < 0) exit();

    if(anak_5 == 0){
        sleep(3);
        execl("/usr/bin/find", "find", "/home/sharira/modul2/animal/", "-type", "f", "-name", "*air*", "-exec", "mv", "-t", "/home/sharira/modul2/air/", "{}", "+", (char *) NULL);
    } else while(wait((&status)) > 0);

    //3d    
    //menghapus file in dir: animal
    anak_6 = fork();

    if(anak_6 < 0) exit();

    if(anak_6 == 0) 
        execl("/bin/sh", "sh", "-c", "rm -f /home/sharira/modul2/animal/*", (char *) NULL);
    else while(wait((&status)) > 0);
        
    //mengapus bird
    anak_7 = fork();
    if(anak_7 < 0) exit();

    if (anak_7 == 0){
        execl("/bin/sh", "sh", "-c", "rm -f /home/sharira/modul2/darat/*bird*", (char *) NULL);
    }   
    
    else while(wait((&status)) > 0);

    //3e
    //input file name format: uid_uid permission_filename
    anak_8 = fork();
    if(anak_8 < 0) exit();
  
    if (anak_8 == 0){
        execl("/bin/sh", "sh", "-c", "ls -la /home/sharira/modul2/air | awk 'NR > 3 {print $3\"_\"substr($1,2,2)\"_\"$9}' > /home/sharira/modul2/air/list.txt", (char *) NULL);
    }   
    
    else while(wait((&status)) > 0);
}
