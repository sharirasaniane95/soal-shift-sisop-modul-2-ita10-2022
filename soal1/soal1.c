#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>

void dwDB(char *database, char *name) {
    pid_t anak_id;
    anak_id = fork();

    if (anak_id == 0) {
        char *argv[8] = {
            "wget", 
            "--no-check-certificate", 
            "-q",
            database, 
            "o",
            name
        };
        
        execv("/bin/wget", argv); 
        exit(EXIT_SUCCESS);
    } else if (anak_id > 0) {
        wait(NULL); 
    }
}



void extract(char *name) {
    pid_t anak_id;
    anak_id = fork();

    if (anak_id == 0) {
        char *argv[4] = {
            "unzip", 
            "-q",
            name
        };

        execv("/usr/bin/unzip", argv);
        exit(EXIT_SUCCESS);
    } else if (anak_id > 0) {
        wait(NULL); 
    }
}


void rm(char *name) {
    pid_t anak_id;
    anak_id = fork();

    if (anak_id == 0) {
        char *argv[4] = {
            "rm", 
            "-r",
            name
        };
        execv("/usr/bin/rm", argv);
        exit(EXIT_SUCCESS);
    } else if (anak_id > 0) {
        wait(NULL); 
    }
}



void makedir(char *fold) {
    pid_t anak_id;
    anak_id = fork();

    if (anak_id == 0) {
        char *argv[2] = {
            "mkdir", 
            "gacha_gacha"
        };
        execv("/usr/bin/mkdir", argv);
        exit(EXIT_SUCCESS);
    } else if (anak_id > 0) {
        wait(NULL); 
    }
}


int main(){
    char *database[2] = {"https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp","https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT"};
    char *name[4] = {"uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp","uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT"};
    char *fold[1] = {"gacha_gacha"};

    makedir(fold[0]);
for (int i=0; i<2; i++){
    pid_t anak_id;
    anak_id =fork();

    if (anak_id ==0){
    dwDB(database[i], name[i]);
    extract(name[i]);
        exit(EXIT_SUCCESS);

    } else if (anak_id > 0) {
        wait(NULL); 
    }
        rm(name[i]);
  
}



}


